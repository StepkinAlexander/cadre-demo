FROM ubuntu:16.10
MAINTAINER Stepkin Alexander sastepkin@yandex.ru

RUN apt update
RUN apt install -y build-essential postgresql-9.5 sudo

RUN service postgresql restart
RUN update-rc.d postgresql enable

RUN sudo -u postgres psql -h 127.0.0.1 -p 5432 -c "UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1';"
RUN sudo -u postgres psql -c "DROP DATABASE template1;"
RUN sudo -u postgres psql -c "CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UNICODE';"
RUN sudo -u postgres psql -c "UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1';"
RUN sudo -u postgres psql -c "VACUUM FREEZE;"

RUN locale-gen "en_US.UTF-8"
RUN dpkg-reconfigure locales
RUN echo "export LC_ALL=en_US.utf8" >> ~/.bashrc
RUN echo "export LANG=en_US.utf8" >> ~/.bashrc
RUN echo "export LANGUAGE=en_US.utf8" >> ~/.bashrc

RUN echo '127.0.0.1 степкин-ас.рф' >> /etc/hosts

RUN mkdir /projects && cd /projects && git clone https://StepkinAlexander@bitbucket.org/StepkinAlexander/cadre.git
RUN cd /projects/cadre && make install

RUN service uwsgi restart
RUN service nginx restart

EXPOSE 80
