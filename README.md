# README #

Web-Приложение на django+postgres+nginx "База Сотрудников компании"

### Что есть? ###

Есть список людей, у каждого человека:
 - Имя
 - Фамилия
 - Отчество
 - Дата рождения
 - Эл. почта
 - Телефон
 - Дата начала работы
 - Дата окончания работы (пустая если всё ещё работает)
 - Должность
 - Отдел

### TODO ###
Сделать заливку начальных данных management.command

### Какие фичи требуется реализовать? ###

1. Django aдминка для редактирования отделов и людей.
2. Страница списка людей с пагинацией и фильтрами по:
- Отделу
- Работает в компании
3. Страница просмотра данных человека (как-нибудь вывести на странице все возможные данные)
4. Страница алфавитного указателя по фамилиям, где сверху семь селекторов из групп букв, при клике на каждый снизу отбражаются фамилии на эти буквы. Обязательные условия:
- Групп не больше семи
- Количество фамилий в каждой группе должно быть максимально одинаковым
- Имя группы должно меняться в зависимости от того фамилии на какую букву содержит
- Буквы в группах не должны пересекаться (А-П, П-Я - неправильно, А-П,Р-Я - правильно)
- JS и всякие аяксы не нужны. Можно предположить что у нас не будет меньше 300 сотрудников. Выглядеть должно примерно так: http://dl2.joxi.net/drive/0005/1273/333049/150616/5eeda07187.jpg
5. Readme по развертыванию
6. unit-тесты
Использовать:
- Generick-view
- Twitter bootstrap

### Как запускать ###

- Django app:
> make start
или
> python3/bin/python manage.py runserver 0:8000
- тесты:
> make test
или
> python3/bin/python manage.py test

### Как деплоить ###

1. клонировать проект, предполагаем что в /projects
2. если 1. клонировался в другую директорию, исправить base в <project_dir>/deploy/etc/uwsgi/sites/cadre.ini
3.
на u1604: make install
на u1404: make install_1404
4. sudo service nginx restart
5. sudo service uwsgi restart

### Проблемы при деплое ###

1. Postgres PG::Error: ERROR: new encoding (UTF8) is incompatible
Решение:
UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1';
DROP DATABASE template1;
CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UNICODE';
UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1';
\c template1
VACUUM FREEZE;
Проблема должна устраниться. Проблема была локализована в докере.

2. __fake__.DoesNotExist: Post matching query does not exist.
Изначально в докере была эта ошибка а в процессе дебаггинга локализовали в:
UnicodeEncodeError: 'ascii' codec can't encode characters in position 103-108: ordinal not in range(128)
Решение:
root@df329ec1fe88:/# python3
Python 3.4.0 (default, Apr 11 2014, 13:05:11)
[GCC 4.8.2] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import codecs
>>> codecs.decode(b'\xe2\x80\x99','utf8')
'\u2019'
>>> exit()
root@df329ec1fe88:/# locale -a
C
C.UTF-8
POSIX
root@df329ec1fe88:/# locale
LANG=
LANGUAGE=
LC_CTYPE="POSIX"
LC_NUMERIC="POSIX"
LC_TIME="POSIX"
LC_COLLATE="POSIX"
LC_MONETARY="POSIX"
LC_MESSAGES="POSIX"
LC_PAPER="POSIX"
LC_NAME="POSIX"
LC_ADDRESS="POSIX"
LC_TELEPHONE="POSIX"
LC_MEASUREMENT="POSIX"
LC_IDENTIFICATION="POSIX"
LC_ALL=
root@df329ec1fe88:/# sudo locale-gen "en_US.UTF-8"
Generating locales...
  en_US.UTF-8... done
Generation complete.
root@df329ec1fe88:/# sudo dpkg-reconfigure locales
Generating locales...
  en_US.UTF-8... up-to-date
Generation complete.
root@df329ec1fe88:/# echo "export LC_ALL=en_US.utf8" >> ~/.bashrc
root@df329ec1fe88:/# echo "export LANG=en_US.utf8" >> ~/.bashrc
root@df329ec1fe88:/# echo "export LANGUAGE=en_US.utf8" >> ~/.bashrc
root@df329ec1fe88:/# source ~/.bashrc
root@df329ec1fe88:/# locale
Для превентивного устранения Dockerfile'ом:
RUN locale-gen "en_US.UTF-8"
RUN dpkg-reconfigure locales
RUN echo "export LC_ALL=en_US.utf8" >> ~/.bashrc
RUN echo "export LANG=en_US.utf8" >> ~/.bashrc
RUN echo "export LANGUAGE=en_US.utf8" >> ~/.bashrc

### Разработка ###

* Разработчик: Стёпкин Александр Сергевич (sastepkin@yandex.ru)