sys_install_1604:
	sudo apt update
	sudo apt install -y python3.5-dev libmysqlclient-dev nginx uwsgi  uwsgi-plugin-python3
	sudo cp -R deploy/etc /
	sudo cp -R deploy/lib /

sys_install_1404:
	sudo apt update
	sudo apt install -y python3.4-dev libmysqlclient-dev nginx uwsgi  uwsgi-plugin-python3
	sudo cp -R deploy/1404/etc /

create_env:
	[ -d python3 ] || virtualenv -p python3 python3
	python3/bin/pip install -r requirements.txt

db_first_create:
	sudo -u postgres psql -c "CREATE USER projuser WITH password 'aq1';"
	sudo -u postgres psql -c "ALTER USER projuser CREATEDB;"
	sudo -u postgres psql -c "CREATE DATABASE cadre OWNER projuser ENCODING 'UTF8';"
	sudo -u postgres psql cadre -c "CREATE SCHEMA IF NOT EXISTS cadre AUTHORIZATION projuser;"
	# создадим тестовую базу
	sudo -u postgres psql -c "CREATE DATABASE test_cadre OWNER projuser ENCODING 'UTF8';"
	sudo -u postgres psql cadre -c "CREATE SCHEMA IF NOT EXISTS test_cadre AUTHORIZATION projuser;"

db_drop:
	sudo -u postgres psql -c "drop DATABASE cadre;"
	sudo -u postgres psql -c "CREATE DATABASE cadre OWNER projuser ENCODING 'UTF8';"

db_fix_sequences:
	sudo -u postgres psql cadre -c "SELECT setval('cadre_departament_id_seq', (SELECT MAX(id) FROM cadre_departament)+1)"
	sudo -u postgres psql cadre -c "SELECT setval('cadre_post_id_seq', (SELECT MAX(id) FROM cadre_post)+1)"
	sudo -u postgres psql cadre -c "SELECT setval('cadre_person_id_seq', (SELECT MAX(id) FROM cadre_person)+1)"
	sudo -u postgres psql cadre -c "SELECT setval('cadre_employee_id_seq', (SELECT MAX(id) FROM cadre_employee)+1)"

dj_migrate:
	python3/bin/python manage.py migrate

dj_create_su:
	python3/bin/python manage.py createsuperuser

install: sys_install_1604 create_env db_first_create dj_migrate db_fix_sequences dj_create_su

install_1404: sys_install_1404 create_env db_first_create dj_migrate db_fix_sequences dj_create_su

db_reset: db_drop dj_migrate db_fix_sequences dj_create_su

start:
	python3/bin/python manage.py runserver

test:
	python3/bin/python manage.py test
