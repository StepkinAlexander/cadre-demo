Django==1.11
psycopg2==2.7.3
pytz==2017.2
# testing
flexmock
pytest>=2.8
pytest-pep8
