# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import operator

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.views.generic import ListView, DetailView
from django.db.models.functions import Substr, Upper
from django.db.models import Q, Count

from .models import Person, Departament, Employee


class EmployeeDetail(DetailView):
    model = Employee
    template_name = "employee_detail.html"


class EmployeeListView(ListView):
    model = Employee
    template_name = "employee_list.html"
    paginate_by = 15

    def get_context_data(self, **kwargs):
        context = super(EmployeeListView, self).get_context_data(**kwargs)
        departament_id = self.request.GET.get('departament')
        if departament_id is not None:
            if departament_id.isdigit():
                try:
                    context['departament'] = Departament.objects.get(pk=departament_id)
                except Departament.DoesNotExist:
                    pass
        context['departament_list'] = Departament.objects.all()
        is_employee = self.request.GET.get('is_employee')
        if is_employee is not None:
            context['is_employee'] = is_employee
        return context

    def get_queryset(self):
        queryset = Employee.objects.all()
        departament_id = self.request.GET.get('departament')
        if departament_id is not None:
            if not departament_id.isdigit():
                return queryset
            try:
                departament = Departament.objects.get(pk=departament_id)
            except Departament.DoesNotExist:
                return queryset
            else:
                queryset = Employee.objects.filter(departament=departament)
        is_employee = self.request.GET.get('is_employee')
        if is_employee is not None:
            if is_employee == "1":
                queryset = queryset.filter(date_fired__isnull=True)
            else:
                queryset = queryset.exclude(date_fired__isnull=True)
        return queryset


class PersonGroupedListView(ListView):
    model = Person
    template_name = "person_list.html"

    letter_group_quantity = 7  # количество групп букв
    max_elems_count = None

    def get_counted_lastnames_list(self):
        """получаем список фамилий с количеством её носителей"""
        return Person.objects \
            .values('last_name') \
            .annotate(cnt_users=Count('id')) \
            .order_by('last_name')

    def get_firstletter_lastnames_counters(self):
        """вычисляем сколько фамилий начинаются на определённую букву"""
        letters_stat_collection = {}
        queryset = self.get_counted_lastnames_list()
        for item in queryset:
            fst_letter = item.get('last_name')[:1]
            if letters_stat_collection.get(fst_letter) is not None:
                letters_stat_collection[fst_letter]['cnt_last_name'] += 1
            else:
                letters_stat_collection[fst_letter] = dict(
                    letter=fst_letter,
                    cnt_last_name=1)
        self.max_elems_count = queryset.count() // self.letter_group_quantity + 1
        letters_stat_list = [letters_stat_collection[key] for key in letters_stat_collection.keys()]
        letters_stat_list.sort(key=operator.itemgetter('letter'))
        return letters_stat_list

    def get_letters_grouped_filters(self):
        def format_letter_block(letter_list):
            letters_label = '%s-%s' % (letter_list[0], letter_list[-1])
            if len(letter_list) == 1:
                letters_label = letter_list[0]
            letter_values = ','.join(letter_list)
            return dict(label=letters_label, value=letter_values)

        in_group_count = 0
        filters = []
        letters = []
        letters_stat_list = self.get_firstletter_lastnames_counters()
        for item in letters_stat_list:
            if len(filters) == self.letter_group_quantity - 1:
                """у нас не более 7 групп, так что остатки, татистически не распределившиеся
                в предыдущие группы падают в 7ю"""
                in_group_count += item.get('cnt_last_name')
                letters.append(item.get('letter'))
            elif item.get('cnt_last_name') >= self.max_elems_count:
                """фамилий на эту букву больше или равно максимальному количеству в блоке:
                делаем сброс накопителей и блок уже сформированный(если уже фомировали),
                блок из этой буквы"""
                if in_group_count > 0:
                    in_group_count = 0
                    letters = []
                    filters.append(format_letter_block(letters))
                filters.append(format_letter_block([item.get('letter')]))
            elif in_group_count + item.get('cnt_last_name') > self.max_elems_count:
                """с этой буквой в блоке будет больше максимального количества:
                сбрасываем накопители, сохраняем подсчитанный блок,
                начинаем накапливать новый блок"""
                if in_group_count > 0:
                    filters.append(format_letter_block(letters))
                    letters = []
                in_group_count = item.get('cnt_last_name')
                letters.append(item.get('letter'))
            else:
                """очередная буква для блока:
                накапливаем"""
                in_group_count += item.get('cnt_last_name')
                letters.append(item.get('letter'))
        if in_group_count > 0:
            filters.append(format_letter_block(letters))
        return filters

    def get_context_data(self, **kwargs):
        context = super(PersonGroupedListView, self).get_context_data(**kwargs)

        context['filter_list'] = self.get_letters_grouped_filters()
        if self.request.GET.get('filter') is not None:
            context['filter'] = self.request.GET.get('filter')

        person_list = Person.objects.all()
        context['person_list'] = person_list
        return context

    def get_queryset(self):
        queryset = Person.objects

        filter = self.request.GET.get('filter')
        if filter is not None:
            filter_list = filter.split(',')
            query = Q()
            for letter in filter_list:
                query = query | Q(last_name__startswith=letter)
            queryset = Person.objects.filter(query)
        return queryset \
            .values('last_name') \
            .annotate(cnt_users=Count('id')) \
            .order_by('last_name')
