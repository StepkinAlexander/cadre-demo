# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
import flexmock

from ..views import PersonGroupedListView
from .utils import MockQueryset


class GetTestCase(TestCase):

	def setUp(self):
		self.client = Client()

	def test_pages(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

		response = self.client.get('/employees/')
		self.assertEqual(response.status_code, 200)

		response = self.client.get('/persons/')
		self.assertEqual(response.status_code, 200)

	def test_trailing_slash(self):
		response = self.client.get('/employees')
		self.assertEqual(response.status_code, 301)

	def test_not_found_page(self):
		response = self.client.get('/bad_url/')
		self.assertEqual(response.status_code, 404)


class PersonListTestCase(TestCase):
	view = None
	queryset_value = [{'last_name': 'Иванов', 'cnt_users': 5},
					  {'last_name': 'Ильин', 'cnt_users': 2},
					  {'last_name': 'Александров', 'cnt_users': 2}]

	def setUp(self):
		self.view = PersonGroupedListView()
		mock_queryset = MockQueryset(self.queryset_value)

		flexmock(PersonGroupedListView) \
			.should_receive('get_counted_lastnames_list') \
			.and_return(mock_queryset)

	def test_mock(self):
		resp = self.view.get_counted_lastnames_list()
		self.assertEqual(len(resp), len(self.queryset_value))

	def test_stage_firstletter_counters(self):
		resp = self.view.get_firstletter_lastnames_counters()
		# ожидается [{'cnt_last_name': 2, 'letter': 'И'}, {'cnt_last_name': 1, 'letter': 'А'}]
		self.assertEqual(len(resp), 2)

	def test_get_filters(self):
		resp = self.view.get_letters_grouped_filters()
		# ожидается [{'value': 'А', 'label': 'А'}, {'value': 'И', 'label': 'И'}]
		self.assertEqual(len(resp), 2)
		self.assertTrue({'value': 'А', 'label': 'А'} in resp)
		self.assertTrue({'value': 'И', 'label': 'И'} in resp)
		self.assertFalse({'value': 'Б', 'label': 'Б'} in resp)
