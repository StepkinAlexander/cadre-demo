# -*- coding: utf-8 -*-


class MockQueryset(object):

    def __init__(self, values_list):
        self.queryset = values_list

    def values_list(self):
        return self.queryset

    def count(self):
        return len(self.queryset)

    def __len__(self):
        return len(self.queryset)

    def __iter__(self):
        for item in self.queryset:
            yield item

    def __repr__(self):
        return '<QuerySet: %s>' % self.queryset
