# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import flexmock

from ..views import PersonGroupedListView
from .utils import MockQueryset


def test_pages(client):
    response = client.get('/')
    assert response.status_code == 200

    response = self.client.get('/employees/')
    assert response.status_code == 200

    response = self.client.get('/persons/')
    assert response.status_code == 200


def test_trailing_slash(client):
    response = client.get('/employees')
    assert response.status_code == 301


def test_not_found_page(client):
    response = client.get('/bad_url/')
    assert response.status_code == 404


queryset_value = [{'last_name': 'Иванов', 'cnt_users': 5},
                  {'last_name': 'Ильин', 'cnt_users': 2},
                  {'last_name': 'Александров', 'cnt_users': 2}]


test_view = PersonGroupedListView()
mock_queryset = MockQueryset(queryset_value)

flexmock(PersonGroupedListView) \
    .should_receive('get_counted_lastnames_list') \
    .and_return(mock_queryset)

def test_mock():
    resp = test_view.get_counted_lastnames_list()
    assert len(resp) == len(queryset_value)


def test_stage_firstletter_counters():
    resp = test_view.get_firstletter_lastnames_counters()
    # ожидается [{'cnt_last_name': 2, 'letter': 'И'}, {'cnt_last_name': 1, 'letter': 'А'}]
    assert len(resp) == 2

def test_get_filters():
    resp = test_view.get_letters_grouped_filters()
    # ожидается [{'value': 'А', 'label': 'А'}, {'value': 'И', 'label': 'И'}]
    assert len(resp) == 2
    assert {'value': 'А', 'label': 'А'} in resp is True
    assert {'value': 'И', 'label': 'И'} in resp is True
    assert {'value': 'Б', 'label': 'Б'} in resp is False
