# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-11 06:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Departament',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField(max_length=255, verbose_name='Название')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name_plural': 'Отделы',
                'verbose_name': 'Отдел',
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='Дата начала работы')),
                ('date_fired', models.DateTimeField(blank=True, null=True, verbose_name='Дата окончания работы')),
                ('departament', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cadre.Departament')),
            ],
            options={
                'ordering': ['id'],
                'verbose_name_plural': 'Сотрудники',
                'verbose_name': 'Сотрудник',
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=30, verbose_name='Имя')),
                ('middle_name', models.CharField(blank=True, max_length=30, null=True, verbose_name='Отчество')),
                ('last_name', models.CharField(max_length=50, verbose_name='Фамилия')),
                ('birth_date', models.DateTimeField(verbose_name='Дата рождения')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Email')),
                ('phone', models.CharField(blank=True, max_length=15, null=True, verbose_name='Телефон')),
            ],
            options={
                'ordering': ['last_name'],
                'verbose_name_plural': 'Карточки',
                'verbose_name': 'Карточка',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField(max_length=255, verbose_name='Название')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name_plural': 'Должности',
                'verbose_name': 'Должность',
            },
        ),
        migrations.AddField(
            model_name='employee',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cadre.Person'),
        ),
        migrations.AddField(
            model_name='employee',
            name='post',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cadre.Post'),
        ),
    ]
