# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Departament(models.Model):
	title = models.TextField('Название', max_length=255)

	class Meta:
		verbose_name = 'Отдел'
		verbose_name_plural = 'Отделы'
		ordering = ['title']

	def __str__(self):
		return self.title


class Post(models.Model):
	title = models.TextField('Название', max_length=255)

	class Meta:
		verbose_name = 'Должность'
		verbose_name_plural = 'Должности'
		ordering = ['title']

	def __str__(self):
		return self.title


class Person(models.Model):
	"""в задаче не было условий управлять полномочиями пользователями,
	потому просто создадим модель с нуля с нужными полями а не будем наследоваться
	от AbstractUser"""
	first_name = models.CharField('Имя', max_length=30)
	middle_name = models.CharField('Отчество', max_length=30, null=True, blank=True)
	last_name = models.CharField('Фамилия', max_length=50)
	birth_date = models.DateTimeField('Дата рождения')
	email = models.EmailField('Email', unique=True)
	phone = models.CharField('Телефон', max_length=15, null=True, blank=True)

	class Meta:
		verbose_name = 'Карточка'
		verbose_name_plural = 'Карточки'
		ordering = ['last_name']

	def __str__(self):
		full_name = '%s %s. %s.' % (self.last_name, self.first_name[:1], self.middle_name[:1])
		return full_name

	def get_full_name(self):
		full_name = '%s %s %s' % (self.last_name, self.first_name, self.middle_name)
		return full_name.strip()


class Employee(models.Model):
	person = models.ForeignKey(Person)
	departament = models.ForeignKey(Departament)
	post = models.ForeignKey(Post)
	date_joined = models.DateTimeField('Дата начала работы', auto_now_add=True)
	date_fired = models.DateTimeField('Дата окончания работы', null=True, blank=True)

	class Meta:
		verbose_name = 'Сотрудник'
		verbose_name_plural = 'Сотрудники'
		ordering = ['id']

	def __str__(self):
		return self.person.get_full_name()
