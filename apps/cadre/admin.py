# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import User, Group

from apps.cadre.models import Departament, Post, Person, Employee


admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.register(Departament)
admin.site.register(Post)
admin.site.register(Person)
admin.site.register(Employee)
